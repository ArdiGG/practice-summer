﻿#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <string.h>
#include <windows.h>
#include "bibl.h"

int main()
{
    extern char res[MAX];
    SetConsoleOutputCP(1251);
    setlocale(LC_ALL, "ukr");
    int day, month;
    printf("Введiть у виглядi цiлих чисел:\nНомер мiсяця: ");
    scanf_s("%d", &month);
    printf("Номер дня: ");
    scanf_s("%d", &day);

    check(day, month);

    change(day, month);

    printf("%s", res);
    return 0;
}